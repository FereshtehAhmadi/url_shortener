from django.shortcuts import render, redirect
from django.views.generic import View

from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from django.conf import settings

from files.models import Files
from files.serializer import FileSerializer

import os


class AllFiles(APIView):
    def get(self, request):
        queryset = Files.objects.all()
        serializer = FileSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class RedirectFiles(View):
    def get(self, request, shorten_link, *args, **kwargs):
        shorten_link = settings.HOST_URL + '/file/' + self.kwargs['shorten_link']
        files_link = Files.objects.filter(files_shorten_link=shorten_link).first().files
        result = files_link.name
        redirect_link = settings.HOST_URL + '/media/' + result
        return redirect(redirect_link)

