from django.db import models
from django.conf import settings
from django.urls import path

import os
from random import choices
from string import ascii_letters


class Files(models.Model):
    files = models.FileField(upload_to='files', blank=True)
    files_shorten_link = models.URLField(blank=True, null=True)

    def shortener(self):
        """ Creates a short link and checks that it is not duplicated...  """
        while True:
            generate_random = ''.join(choices(ascii_letters, k=5))
            new_link = settings.HOST_URL + '/file/' + generate_random
            
            if not Files.objects.filter(files_shorten_link=new_link).exists():
                break
        return new_link
    
    def save(self, *args, **kwargs):
        
        if not self.files_shorten_link:
            new_link = self.shortener()
            self.files_shorten_link = new_link
        
        return super().save(*args, **kwargs)

