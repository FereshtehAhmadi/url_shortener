from django.urls import path, include
from files import views


urlpatterns = [
    path('all_files/', views.AllFiles.as_view(), name='all_files'),
    path('<str:shorten_link>', views.RedirectFiles.as_view(), name='redirect_files'),
]
